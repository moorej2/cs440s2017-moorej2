#include<stdio.h>
#include<string.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>

// compile on MacOS with: gcc -o threads howToThread.c
// compile on Linux with: g++ -pthread howToThread.c

pthread_t tid[2];

void* doSomeThing(void *arg)
{
    unsigned long i = 0;
    pthread_t id = pthread_self(); //pthread_self - obtain ID of the calling thread

// The pthread_equal() function returns a non-zero value if tread1 and thread2 are equal; otherwise, zero is returned.

    if(pthread_equal(id,tid[0]))
    {
        printf("\n First thread processing\n");
    }
    else
    {
        printf("\n Second thread processing\n");
    }

    for(i=0; i<(0xFFFFFFFF);i++);

// For diagnostic purposes
//    {
//      printf("\n value of i =%lu",i);
//    }
    return NULL;
}

int main(void)
{
    int i = 0;
    int err;

//    while(i < 2)
    while(i < 2)
    {
        err = pthread_create(&(tid[i]), NULL, &doSomeThing, NULL);
        if (err != 0)
            printf("\ncan't create thread :[%s]", strerror(err));
        else
            printf("\n %i: Thread created successfully\n",i);

        i++;
    }

    sleep(5);
    return 0;
}
