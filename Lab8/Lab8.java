import java.io.*;
import java.util.*;

public class Lab8 {


  public static void main (String[]args){
    String state;
    String unstable;
    String stable;

    Scanner scan = new Scanner(System.in);
    System.out.println(" Welcome to Learning the Bankers Algorithm!!!!\n");
    System.out.println(" There are 2 examples of the Algorithm \n");
    System.out.println("        Unstable State | Stable State\n ");
    System.out.println(" Please choose one \n ");
    state = scan.nextLine();
/** Example of unstable method of bankers algorithm */
if (state.equals("unstable")){
  System.out.println(" ASSUME that we have $30");
  System.out.println("--------This is a Example of a Unstable State-------\n");
  System.out.println("   PLAYERS    |  1  |   2  |  3  |  4  |            \n");
  System.out.println("________________________________________\n");
  System.out.println("   Requried   | $5  |  $10 | $15 | $20 |            \n");
  System.out.println("Already Given | $3  |  $5  | $14 | $10 |            \n");
  System.out.println("   Needs      | $2  |  $5  | $1  | $10 |            \n");
// This method will create a deadlock because $30 is not enough money for Allocation
// $30-3-5-14-10= (-2)
}
/** Example of unstable method of bankers algorithm */
if (state.equals("stable")){
  System.out.println(" ASSUME that we have $33");
  System.out.println("--------This is a Example of a Stable State-------\n");
  System.out.println("   PLAYERS    |  1  |   2  |  3  |  4  |            \n");
  System.out.println("________________________________________\n");
  System.out.println("   Requried   | $5  |  $10 | $15 | $20 |            \n");
  System.out.println("Already Given | $3  |  $5  | $14 | $10 |            \n");
  System.out.println("   Needs      | $2  |  $5  | $1  | $10 |            \n");
  // This method will not create a deadlock because $33 is just enough money for Allocation
  // $33-3-5-14-10= 1
  // That last "1" resource would go to player 3, which will then allow for other players to run thier processes

}
}
}
