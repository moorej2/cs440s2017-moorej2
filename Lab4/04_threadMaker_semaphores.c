#include<stdio.h>
#include<string.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>
#include <semaphore.h>

#define NUM_THREADS     2

//compile on linux: gcc -pthread 04_leftRight_noSleep.c
//

sem_t leftsem, rightsem;


void *process(void *processed)
{
  long y;
  long tid;
  tid = (long)processed;
    for (;;) {
        sem_wait(&leftsem);
        sleep(1); // allows to see the individual print lines
        printf(" Process #%ld     \n", y);
        sem_post(&leftsem);
	}
}

void *threaded(void *processed)
{
  long z;
  long tid;
  tid = (long)processed;
	for (;;){
        	sem_wait(&rightsem);
          sleep(1); // allows to see the individual print lines
        	printf("          Thread #%ld \n", z);
        	sem_post(&rightsem);
	}
}

int main (int argc, char *argv[])
{
  pthread_t mythread[NUM_THREADS];
  int xz;
  long y;
  long z;
  for(y=1; y<NUM_THREADS; y++){

      }
	sem_init(&leftsem, 0, 0);
	sem_init(&rightsem, 0, 1);
	pthread_t process_thread, threaded_thread;
	pthread_create(&process_thread, NULL, process, NULL);
	pthread_create(&threaded_thread, NULL, threaded, NULL);
	pthread_join(process_thread, NULL);
	pthread_join(threaded_thread, NULL);
	printf("\n");
	return 0;

}
